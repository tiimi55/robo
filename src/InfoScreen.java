import lejos.nxt.LCD;

public class InfoScreen implements Runnable {
	Steering ohjaus;
	ObjectDetector od;
	StopWatch kello;

	public InfoScreen(ObjectDetector od, Steering ohj, StopWatch kello) {
		this.od = od;
		this.ohjaus = ohj;
		this.kello = kello;
	}
/**
 * Näyttää NXT:n ruudulla tietoa mm. robotin sensoreilta ja moottorilta.
 * Lisäksi näyttää lopuksi ajan, joka radan kulkemiseen robotilta kului.
 */
	private void drawScreen() {
//		while (Controller.getVaihe() != 4) {
		LCD.clear();
		while (true) {

			LCD.drawString("Musta: " + String.valueOf(ohjaus.getBlack()), 0, 0);
			LCD.drawString("Valkoinen: " + String.valueOf(ohjaus.getWhite()), 0, 1);
			LCD.drawString("Vaihe : " + Controller.getVaihe(), 0, 2);
			LCD.drawString(
					"Valoarvo: " + String.valueOf(ohjaus.cs.getLightValue()),
					0, 3);
			LCD.drawString("Etaisyys: " + (float) od.sonar.getDistance(), 0, 4);
			LCD.drawString(
					"Power Left: "
							+ String.valueOf(ohjaus.motorleft
									.getPower()), 0, 5);
			LCD.drawString(
					"Power Right: "
							+ String.valueOf(ohjaus.motorright
									.getPower()), 0, 6);
			if (Controller.getVaihe() == 4) {
				LCD.clear();
				LCD.drawString("Maali!", 0, 0);
				LCD.drawString("Aikaa meni", 0, 1);
				LCD.drawString(kello.timeTaken(),0,2);
				break;
			}
		}
	}
	
	public void run() {
		drawScreen();
	}

}
