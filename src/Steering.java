import lejos.nxt.ColorSensor;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;

public class Steering implements Runnable {

	NXTMotor motorleft = new NXTMotor(MotorPort.C);
	NXTMotor motorright = new NXTMotor(MotorPort.B);
	NXTRegulatedMotor motorrightReg = new NXTRegulatedMotor(MotorPort.B);
	NXTRegulatedMotor motorleftReg = new NXTRegulatedMotor(MotorPort.C);
	ColorSensor cs = new ColorSensor(SensorPort.S1);

	private int black;
	private int white;
	private float kerroin;
	private float hidastus = 1;
	private int normalizedValue;
	private boolean puoli = true; // seurattava viivan reuna. true = oikea
									// reuna, false = vasen reuna

	/**
	 * Palauttaa viivan seurauksessa käytettävän viivan valoarvon.
	 * 
	 * @return Kokonaisluku väliltä 0-100
	 */
	public int getBlack() {
		return black;
	}

	/**
	 * Asettaa seurattavan viivan valoarvon
	 * 
	 * @param black
	 *            Arvo v�lilt� 0-100, oltava pienempi kuin valkoisen arvo, jotta
	 *            ohjelma toimii
	 */
	public void setBlack(int black) {
		this.black = black;
	}

	/**
	 * Palauttaa viivan seurauksessa käytettävän viivan ulkopuolisen alueen
	 * valoarvon.
	 * 
	 * @return Kokonaisluku väliltä 0-100
	 */
	public int getWhite() {
		return white;
	}

	/**
	 * Asettaa seurattavan viivan ulkopuolisen valoarvon
	 * 
	 * @param white
	 *            Arvo v�lilt� 0-100, oltava suurempi kuin musta arvo, jotta
	 *            ohjelma toimii
	 */
	public void setWhite(int white) {
		this.white = white;
	}

	/**
	 * Palauttaa kertoimen, jolla moottoreiden nopeutta säädetään.
	 * 
	 * @return kerroin (float).
	 */
	public float getKerroin() {
		return kerroin;
	}

	/**
	 * Asettaa kertoimen, jolla moottoreiden nopeutta säädetään.
	 * 
	 * @param kerroin
	 *            float, toimii parhaiten, mikäli > 1. Pienemmät arvot saavat
	 *            robotin kulkemaan todella hitaasti.
	 */
	public void setKerroin(float kerroin) {
		this.kerroin = kerroin;
	}

	/**
	 * Palauttaa viivan puolen, jota botti seuraa
	 * 
	 * @return puoli, true = oikea, false = vasen
	 */
	public boolean getPuoli() {
		return puoli;
	}

	/**
	 * Asettaa viivan puolen, jota botti seuraa,
	 * 
	 * @param puoli
	 *            true = oikea puoli, false = vasen puoli
	 */
	public void setPuoli(boolean puoli) {
		this.puoli = puoli;
	}

	int getNormalizedValue() {
		return normalizedValue;
	}

	void setNormalizedValue(int normalizedValue) {
		this.normalizedValue = normalizedValue;
	}

	/**
	 * Asettaa PC-käyttöliittymästä USB-yhteyden kautta saadun nopeuden.
	 * 
	 * @param vaihde
	 *            <br>
	 *            1 = 60% maksiminopeudesta <br>
	 *            2 = 80% maksiminopeudesta <br>
	 *            3 = Maksiminopeus (oletus)
	 */
	public void setVaihde(int vaihde) {
		if (vaihde == 1) {
			hidastus = 0.6F;
		}
		if (vaihde == 2) {
			hidastus = 0.8F;
		} else
			hidastus = 1;
	}

	public int getVaihde() {
		int v;
		if (hidastus == 0.6F) {
			v = 1;
		}
		if (hidastus == 0.8F) {
			v = 2;
		} else
			v = 3;
		return v;
	}

	/**
	 * Huolehtii botin viivanseurauksen ja esteenväistön logiikasta. Kutsutaan
	 * run()-metodista.
	 */
	public void ajo() {
		while (true) {
			while (Controller.getVaihe() == 1 || Controller.getVaihe() == 3) {
				viivanSeuraus();
			}

			while (Controller.getVaihe() == 2) {
				vaista();
			}

			while (Controller.getVaihe() == 4) {
				motorleft.stop();
				motorright.stop();

				break;
			}
		}
	}

	/**
	 * Logiikka viivanseuraukselle ja puoleisuudelle (hakee kumman puoleista
	 * viivaa seurataan)
	 */
	private void viivanSeuraus() {
		int lvaluedifference;
		float lpower;
		float rpower;

		// oikeanpuoleinen seuraus
		if (getPuoli()) {
			if (cs.getLightValue() > white - 2) {
				lpower = -20;
			} else {
				lpower = (white - cs.getLightValue()) * kerroin;
			}
			if (cs.getLightValue() < black + 2) {
				rpower = -20;
			} else {
				rpower = (cs.getLightValue() - black) * kerroin;
			}
		}

		// vasemmanpuoleinen seuraus
		else {
			if (cs.getLightValue() > white - 2) {
				rpower = -20;
			} else {
				rpower = (white - cs.getLightValue()) * kerroin;

			}
			if (cs.getLightValue() < black + 2) {
				lpower = -20;
			} else {
				lpower = (cs.getLightValue() - black) * kerroin;
			}

		}

		lvaluedifference = (white - cs.getLightValue()) - (cs.getLightValue() - black);
		if (Math.abs(lvaluedifference) < 21) {
			float maxSpeed = 100;
			motorright.setPower((int) (maxSpeed * hidastus));
			motorleft.setPower((int) (maxSpeed * hidastus));
		} else {
			motorright.setPower((int) (rpower * hidastus));
			motorleft.setPower((int) (lpower * hidastus));
		}
	}

	/**
	 * Väistää esteen. Kääntyy ensin vasemmalle tai oikealle riippuen
	 * seurattavasta viivan puolesta ja tekee sitten kaarteen samalta puolelta.
	 * Botti kaartaa kunnes löytää jälleen viivan. Kutsutaan ajo()-metodista.
	 * 
	 */
	private void vaista() {
		long now = System.currentTimeMillis();
		long dt = 0;

		while (dt < 700) {
			if (getPuoli()) {
				motorleft.setPower(80);
				motorright.setPower(20);
				dt = System.currentTimeMillis() - now;
			} else {
				motorright.setPower(80);
				motorleft.setPower(20);
				dt = System.currentTimeMillis() - now;

			}
		}

		// oikeanpuoleinen v�ist�
		{
			now = System.currentTimeMillis();
			dt = 0;
			int outerWheel = 70;
			int innerWheel = 45;

			if (getPuoli()) {

				while (normalizeLightValue(cs.getLightValue()) > 30 && dt < 2000) {
					motorright.setPower(outerWheel);
					motorleft.setPower(innerWheel);
				}
				Controller.setVaihe(3);
			}

			// vasemmanpuoleinen v�ist�
			else {
				while (normalizeLightValue(cs.getLightValue()) > 30 && dt < 2000) {
					motorright.setPower(innerWheel);
					motorleft.setPower(outerWheel);
				}
				Controller.setVaihe(3);

			}
		}
	}

	/**
	 * Palauttaa normalisoidun valosensorin arvon
	 * @param value Normalisoitava valosensorin arvo.
	 * @return 0-100 välille normalisoitu kokonaisluku.
	 */
	private int normalizeLightValue(int value) {
		normalizedValue = (int) ((value - black) * (100 - 0) / (white - black));
		//setNormalizedValue((int) ((value - black) * (100 - 0) / (white - black)));
		return normalizedValue;
	}

	public void run() {

		ajo();
	}

}
