

public class Controller {	

	ObjectDetector od;
	Steering ohjaus;
	private static int vaihe = 0;	
	public Controller(ObjectDetector od, Steering ohjaus) {
		this.od = od;
		this.ohjaus = ohjaus;
	}

	/**
	 * Palauttaa vaiheen, jonka mukaan toimitaan
	 * 
	 * @param vaihe <br> 0 = alku, ei liiku <br>
	 *				1 = viivan seuraus <br>
	 *				2 = esteen v�ist� <br>
	 *				3 = viivan seuraus esteen v�ist�n j�lkeen <br>
	 *				4 = lopetus  <br>            
	*/
	public static synchronized int getVaihe() {
		return vaihe;
	}
	/**
	 * Asettaa vaiheen, jonka mukaan toimitaan
	 * 
	 * @param vaihe <br> 0 = alku, ei liiku <br>
	 *				1 = viivan seuraus <br>
	 *				2 = esteen v�ist� <br>
	 *				3 = viivan seuraus esteen v�ist�n j�lkeen <br>
	 *				4 = lopetus  <br>            
	*/
	public static synchronized void setVaihe(int vaihe) {
		Controller.vaihe = vaihe;
	}
}
