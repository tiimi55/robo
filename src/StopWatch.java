import lejos.nxt.Sound;

public class StopWatch implements Runnable {
    
     private long startTime;
     private long stopTime;
     private long timediff;

     
    // KOODIN MALLI http://stackoverflow.com/a/21322238
     
    public void startTiming() //Aloittaa ajastimen
    {
        startTime = System.currentTimeMillis();
        Sound.beep();
        while(true) {
        	if (Controller.getVaihe() == 4){
        		stopTiming();
        		break;
        	}        }
    }

    public void stopTiming() //Stoppaa ajastimen
    {
        stopTime = System.currentTimeMillis();
        Sound.beep();
    }
    

    public String timeTaken() //Kirjoittaa ajastimen muotoon minuutit:sekunnit:millisekunnit
    {
        timediff = (stopTime - startTime);
        
        //ALGORITMI L�HTEEST� http://stackoverflow.com/q/10874048
        long x = timediff;
        int h = (int) (x / (60*60*1000));
        x = (int) (x - h*(60*60*1000));
        int m = (int) (x / (60*1000));
        x = (int) (x - m*(60*1000));
        int s = (int) (x / 1000);
        x = (int) (x - s*1000);
    
    String time = m + " min, " + s + "." + x + " sec";
        return time;
    }
    public void run() {
    	startTiming();
    	
    }
    
}