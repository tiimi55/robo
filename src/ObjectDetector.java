import lejos.nxt.*;

public class ObjectDetector implements Runnable {

	UltrasonicSensor sonar = new UltrasonicSensor(SensorPort.S4);
	private int etaisyys = 28;

	public int getEtaisyys() {
		return etaisyys;
	}

	public void setEtaisyys(int etaisyys) {
		this.etaisyys = etaisyys;
	}

   /** 
	* Esteen havaitseminen. Jos este havaitaan etaisyys muuttujaa pienemmällä
	* arvolla, vaihdetaan Controller.setVaihe (state) yhdellä eteenpäin.
	*/
	
	// Tämä voisi olla private, eikö totta?
	public synchronized void detect() {

		while (true) {

			while (Controller.getVaihe() == 1 || Controller.getVaihe() == 3) {

				if (sonar.getDistance() < getEtaisyys()) {
					Sound.beep();
					Controller.setVaihe(Controller.getVaihe() + 1);
				}
			}
		}
	}

	public void run() {
		detect();
	}
}
