import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Sound;
import lejos.nxt.Motor;
import lejos.util.Delay;

/**
 * Tämä on Lego NXT:lle toteutettu viivanseuraus -ohjelmisto
 * 
 * 
 * @author Petri L�ytynoja, Ville Kortesalmi, Anssi Rajala, Emma Helenius, Antti
 *         Pohja
 * @version 0.5  
 * @since 2015-04-01
 */
public class Main {
	static int black = 20;
	static int white = 60;
	static float kerroin = 1;

	static ObjectDetector od = new ObjectDetector();
	static Steering ohjaus = new Steering();
	static Controller ctrl = new Controller(od, ohjaus);
	static StopWatch kello = new StopWatch();
	static InfoScreen infoscr = new InfoScreen(od, ohjaus, kello);

	public static void main(String[] args) throws InterruptedException {

		LCD.drawString("Paina ENTER usb-", 0, 0);
		LCD.drawString("yhteydelle", 0, 1);
		LCD.drawString("Paina MUU(mi)", 0, 3);
		LCD.drawString("alkuasetuksille", 0, 4);

		Button.waitForAnyPress();
		if (Button.ENTER.isDown()) {
			LCD.clear();
			LCD.drawString("Odottaa USB:t'a'", 0, 0);
			USBReceiver usbi = new USBReceiver(ohjaus, od);
			Thread usb = new Thread(usbi);
			usb.start();
			Button.waitForAnyPress();
		}

		LCD.clear();
		alkuAsetukset();

		Thread ohjthread = new Thread(ohjaus);
		Thread objThread = new Thread(od);
		Thread infoThread = new Thread(infoscr);
		Thread kelloThread = new Thread(kello);

		for (int i = 5; i > 0; i--) {
			LCD.clear();
			LCD.drawString(i + " sekuntia lahtoon!", 0, 0);
			Thread.sleep(1000);
		}

		ohjthread.start();
		objThread.start();
		infoThread.start();
		kelloThread.start();
		Controller.setVaihe(1);
		Button.waitForAnyPress();
	}

	/**
	 * Automaattinen kalibrointi. Botti asetetaan mustalle viivalle, jonka
	 * jälkeen se lukee ensin mustan arvon ja tämän jälkeen valkoisen arvon.
	 * Näiden perusteella lasketaan vielä kerroin, jota käytetään moottoreiden
	 * ohjauksessa.
	 */
	static void alkuAsetukset() {
		try {

			ohjaus.cs.setFloodlight(true);
			haeMustanArvo();
			Motor.C.rotate(220);

			haeValkoisenArvo();
			Motor.C.rotate(-220);
			Motor.C.flt();

			kerroin = (100 / (white - black) * .9f);

		} catch (Exception e) {
			LCD.clear();
			System.out.println("Virheelliset valoarvot. \nPaina nappia \nottaaksesi asetukset\nuudestaan");
			Button.waitForAnyPress();
			alkuAsetukset();
		}

		ohjaus.setKerroin(kerroin);
	}

	static void haeMustanArvo() {
		Delay.msDelay(200);
		LCD.drawString("Lue mustan arvo", 0, 0);
		black = ohjaus.cs.getLightValue();
		ohjaus.setBlack(black);
		Sound.buzz();
		Delay.msDelay(1000);
	}

	static void haeValkoisenArvo() {
		LCD.drawString("Lue valkoisen arvo", 0, 0);
		white = ohjaus.cs.getLightValue();
		ohjaus.setWhite(white);
		Sound.buzz();
		Delay.msDelay(1000);
	}
}
