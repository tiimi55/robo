import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.LCD;
import lejos.nxt.comm.USB;
import lejos.nxt.comm.USBConnection;

public class USBReceiver implements Runnable {

	USBConnection conn = USB.waitForConnection();
	DataOutputStream dataOut = conn.openDataOutputStream();
	DataInputStream dataIn = conn.openDataInputStream();

	ObjectDetector od;
	Steering ohjaus;
	boolean puoli;
	int nopeus;
	int etaisyys;

	// @Override
	public void run() {
		LCD.clear();
		LCD.drawString("Vastaanotettu:", 0, 0);
		LCD.drawString("Paina nappia \naloittaaksesi \nohjelman.", 0, 5);
		
		USBvastaanotto();
		USBlahetys();
		// conn.close();
	}

	public USBReceiver(Steering ohjaus, ObjectDetector od) {
		this.ohjaus = ohjaus;
		this.od = od;
		puoli = ohjaus.getPuoli();
		nopeus = ohjaus.getVaihde();
		etaisyys = od.getEtaisyys();
	}

	public void USBvastaanotto() {

		try {
			puoli = dataIn.readBoolean();
			nopeus = dataIn.readInt();
			etaisyys = dataIn.readInt();
			ohjaus.setPuoli(puoli);
			ohjaus.setVaihde(nopeus);
			od.setEtaisyys(etaisyys);
			
		} catch (Exception e) {

		}

		LCD.drawString(puoli + "," + nopeus + "," + etaisyys, 0, 1);	
	}

	public void USBlahetys() {
		puoli = ohjaus.getPuoli();
		nopeus = ohjaus.getVaihde();
		etaisyys = od.getEtaisyys();
		try {
			dataOut.writeBoolean(puoli);
			dataOut.flush();
			dataOut.writeInt(nopeus);
			dataOut.flush();
			dataOut.writeInt(etaisyys);
			dataOut.flush();
			dataOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LCD.drawString("L:" + puoli + "," + nopeus + "," + etaisyys, 0, 3);
	}
}
